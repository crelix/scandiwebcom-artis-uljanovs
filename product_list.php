<?php

	require_once("config.php");
	require_once("class.product.php");

	$product = new PRODUCT();

	if(isset($_GET['deleteProduct'])){
		$id = $_GET['id'];
		$product->deleteProduct();
	}

?>

<!DOCTYPE html>
<html lang="eng">
<head>

  <!-- META TAGS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- LINKS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

  <title>Scandiweb Task</title>

  <!-- STYLE -->
  <style>
    body{
      background-color: #DCD2CB;
			font-family: Helvetica;
    }
    hr{
      background-color: #000000;
    }
    #content{
      margin: 5%;
    }
    #head{
      height: 30px;
    }
    #title{
      font-size: 24px;
    }
    #actions{
      float: right;
    }
		#menu{
			margin-left: 28%;
			margin-top: 30px;
		}
		#footer{
			float: right;
			font-size: 14px;
			font-weight: bold;
		}
		.message{
			width: 35%;
			height: 50px;
			margin-left: 20%;
			text-align: center;
			margin-top:-5px;
		}
		.hidden{
			display: none;
		}
    .selectFields{
      vertical-align: middle;
    }
    .product{
      border: 2px solid #000000;
			width: 20%;
			line-height: 30px;
			margin-right: 2.5%;
			margin-left: 2.5%;
			margin-top: 20px;
    }
		.panel{
			text-align: center;
		}
		.checkboxProduct{
			margin-left: 2%;
		}
  </style>

</head>
<body>

<!-- MAIN PAGE -->
<div id="content">
  <div id="head">
    <span id="title">Product List</span>
		<div class="message hidden" id="message">
			<p id="messageLabel"></p>
		</div>
    <div id="actions">
      <select id="selectAction" class="selectFields">
        <option>Select Action</option>
        <option value="delete">Mass Delete Action</option>
      </select>
      <button class="btn btn-primary btn-sm" id="btn-action">Apply</button>
    </div>
  </div>
  <hr>
	<div id="menu">
	<?php if(isset($_SESSION["sortby"])){ ?>

			<!-- SORT BY ID-->
			<?php if($_SESSION["sortby"] == "id DESC"){ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyId"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">ID</button></a>
			<?php }else{ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyId"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">ID</button></a>
			<?php } ?>

			<!-- SORT BY SKU -->
			<?php if($_SESSION["sortby"] == "sku"){ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbySKU"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">SKU</button></a>
			<?php }else{ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbySKU"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">SKU</button></a>
			<?php } ?>

			<!-- SORT BY NAME -->
			<?php if($_SESSION["sortby"] == "name"){ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyName"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">Name</button></a>
			<?php }else{ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyName"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Name</button></a>
			<?php } ?>

			<!-- SORT BY PRICE -->
			<?php if($_SESSION["sortby"] == "price"){ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyPrice"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">Price</button></a>
			<?php }else{ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyPrice"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Price</button></a>
			<?php } ?>

			<!-- SORT BY TYPE -->
			<?php if($_SESSION["sortby"] == "type"){ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyType"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">Type</button></a>
			<?php }else{ ?>
				<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyType"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Type</button></a>
			<?php }
		}else{ ?>
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyId"><button class="btn btn-danger" style="margin-right:5px; margin-top:-50px; width:100px">ID</button></a>
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbySKU"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">SKU</button></a>
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyName"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Name</button></a>
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyPrice"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Price</button></a>
			<a href="<?php echo $_SERVER['PHP_SELF']; ?>?action=sortbyType"><button class="btn btn-primary" style="margin-right:5px; margin-top:-50px; width:100px">Type</button></a>
		<?php } ?>
	</div>
	<div class="row">
    <?php $product->selectProducts(); ?>
	</div>
	<hr>
	<div id="footer">
		Scandiweb task / Artis Uljanovs / 28.09.2017
	</div>
</div>

<!-- SCRIPTS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
	$(function(){

		var e = document.getElementById( "selectAction" );

		$("#btn-action").click(function(){
			if(e.options[ e.selectedIndex ].value == "delete"){
				var checked = $('.checkboxProduct:checked');
				var id = checked.map(function() {
        	return this.value;
    		}).get().join(",");
    		if (id) {
					checked.closest(".product").remove();
					$.ajax(
						{ url: "<?php echo $_SERVER['PHP_SELF']; ?>?deleteProduct=true?action=select&id=" + id,
							type: "get",
							success: function(result){
								document.getElementById('message').style.display = 'inline-block';
								document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">You have successfully deleted these products!</div>';
							}
					});
    		}else{
					document.getElementById('message').style.display = 'inline-block';
					document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">You have to select a product!</div>';
				}
			}else{
				document.getElementById('message').style.display = 'inline-block';
				document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">You have to select an action!</div>';
			}
		});
	});
</script>

</body>
</html>
