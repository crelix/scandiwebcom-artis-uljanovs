-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 28, 2017 at 05:51 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiweb`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(255) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` float NOT NULL,
  `type` varchar(9) NOT NULL,
  `size` float DEFAULT NULL,
  `height` float DEFAULT NULL,
  `width` float DEFAULT NULL,
  `length` float DEFAULT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `price`, `type`, `size`, `height`, `width`, `length`, `weight`) VALUES
(1, 'PMZ0001', 'Silent Book', 10.99, 'Book', NULL, NULL, NULL, NULL, 2.2),
(2, 'PMZ0002', 'Silent Book 2', 16.51, 'Book', NULL, NULL, NULL, NULL, 1.7),
(3, 'PMZ0003', 'Silent Book 3', 22.99, 'Book', NULL, NULL, NULL, NULL, 2.7),
(4, 'PMZ0004', 'Silent Book 4', 40, 'Book', NULL, NULL, NULL, NULL, 2.7),
(5, 'KXD0001', 'Movie Time', 19.99, 'DVD-Disc', 2307, NULL, NULL, NULL, NULL),
(6, 'KXD0002', '3D Nature', 78.99, 'DVD-Disc', 20000, NULL, NULL, NULL, NULL),
(7, 'KXD0003', '90\'s POP', 5.99, 'DVD-Disc', 698, NULL, NULL, NULL, NULL),
(8, 'KXD0004', 'POST 4 (Reborn)', 23.49, 'DVD-Disc', 1492, NULL, NULL, NULL, NULL),
(9, 'MEB0001', 'Oak Chair', 289.99, 'Furniture', NULL, 40, 30, 100, NULL),
(10, 'MEB0002', 'Home Decor', 4.99, 'Furniture', NULL, 18, 22, 26, NULL),
(11, 'MEB0003', '16\'s century wardrobe', 8000, 'Furniture', NULL, 180, 76, 265, NULL),
(12, 'MEB0004', 'Wooden Table', 120.99, 'Furniture', NULL, 80, 80, 120, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sku` (`sku`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
