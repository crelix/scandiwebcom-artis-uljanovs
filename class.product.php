<?php

require_once('config.php');

class PRODUCT{

	private $conn;

	public function __construct(){
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
  }

	public function runQuery($sql){
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}

	///   PRODUCT   ///

	public function selectProducts(){
		try{

			if(isset($_SESSION["sortby"])){
				$sortby = $_SESSION["sortby"];
			}else{
				$sortby = "id DESC";
			}

			$stmt = $this->conn->prepare("SELECT * FROM products ORDER BY $sortby");
      $stmt->execute();
      $result = $stmt->fetchAll();

			if((isset($_GET['action']) ? $_GET['action'] : null) == "sortbyId"){
				$_SESSION["sortby"] = "id DESC";
				?><script> location.replace("<?php echo $_SERVER['PHP_SELF']; ?>"); </script><?php
			}

			if((isset($_GET['action']) ? $_GET['action'] : null) == "sortbySKU"){
				$_SESSION["sortby"] = "sku";
				?><script> location.replace("<?php echo $_SERVER['PHP_SELF']; ?>"); </script><?php
			}

			if((isset($_GET['action']) ? $_GET['action'] : null) == "sortbyName"){
				$_SESSION["sortby"] = "name";
				?><script> location.replace("<?php echo $_SERVER['PHP_SELF']; ?>"); </script><?php
			}

			if((isset($_GET['action']) ? $_GET['action'] : null) == "sortbyPrice"){
				$_SESSION["sortby"] = "price";
				?><script> location.replace("<?php echo $_SERVER['PHP_SELF']; ?>"); </script><?php
			}

			if((isset($_GET['action']) ? $_GET['action'] : null) == "sortbyType"){
				$_SESSION["sortby"] = "type";
				?><script> location.replace("<?php echo $_SERVER['PHP_SELF']; ?>"); </script><?php
			}

      foreach($result as $product){ ?>
        <div class="product">
					<input class="checkboxProduct" type="checkbox" name="deleteProduct" value="<?php echo $product['id'];?>">
					<div id="panel_<?php echo $product['id'];?>">
						<div class="thumbnail">
							<div class="panel">
								<?php echo $product['sku']; ?>
							</div>
              <div class="panel">
								<?php echo $product['name']; ?>
							</div>
              <div class="panel">
                <?php echo $product['price'] . ' $'; ?>
              </div>
              <div class="panel">
								<?php
                if($product['type'] == 'DVD-Disc'){
                  echo 'Size: ' . $product['size'] . ' MB';
                }
                if($product['type'] == 'Book'){
                  echo 'Weight: ' . $product['weight'] . ' KG';
                }
                if($product['type'] == 'Furniture'){
                  echo 'Dimensions: ' . $product['height'] . 'x' . $product['width'] . 'x' . $product['length'];
                }
                ?>
							</div>
						</div>
					</div>
				</div>

				<?php
      	}
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function insertProduct($sku,$name,$price,$type,$size,$height,$width,$length,$weight){
		try{

			$stmt = $this->conn->prepare("INSERT INTO products (sku,name,price,type,size,height,width,length,weight)
			VALUES(:sku, :name, :price, :type, :size, :height, :width, :length, :weight)");

			$stmt->bindparam(":sku", $sku);
			$stmt->bindparam(":name", $name);
			$stmt->bindparam(":price", $price);
			$stmt->bindparam(":type", $type);
			$stmt->bindparam(":size", $size);
			$stmt->bindparam(":height", $height);
			$stmt->bindparam(":width", $width);
			$stmt->bindparam(":length", $length);
			$stmt->bindparam(":weight", $weight);

			$stmt->execute();

		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function deleteProduct(){
		try{

			$product_id = explode(',', $_GET['id']);
			$placeholders = implode(', ', array_fill(0, count($product_id), '?'));

			$stmt = $this->conn->prepare("DELETE FROM products WHERE id IN ($placeholders)");
			$stmt->execute($product_id);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

	public function checkProduct(){
		try{

			$sku = $_GET['sku'];

      $stmt = $this->conn->prepare("SELECT count(*) FROM products WHERE sku = '$sku' ");
      $stmt->execute();
      $result = $stmt->fetchColumn();

			echo $result;
			exit();
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
	}

}
?>
