<?php

	require_once("config.php");
	require_once("class.product.php");

	$product = new PRODUCT();

  if((isset($_GET['action']) ? $_GET['action'] : null) == "addProduct"){
    if($_POST['type'] == "DVD-Disc"){
      $sku = strip_tags($_POST['sku']);
      $name = strip_tags($_POST['name']);
      $price = strip_tags($_POST['price']);
      $type = strip_tags($_POST['type']);
      $size = strip_tags($_POST['size']);

      $product->insertProduct($sku,$name,$price,$type,$size,null,null,null,null);
    }
    else if($_POST['type'] == "Book"){
      $sku = strip_tags($_POST['sku']);
      $name = strip_tags($_POST['name']);
      $price = strip_tags($_POST['price']);
      $type = strip_tags($_POST['type']);
      $weight = strip_tags($_POST['weight']);

      $product->insertProduct($sku,$name,$price,$type,null,null,null,null,$weight);
    }
    else if($_POST['type'] == "Furniture"){
      $sku = strip_tags($_POST['sku']);
      $name = strip_tags($_POST['name']);
      $price = strip_tags($_POST['price']);
      $type = strip_tags($_POST['type']);
      $height = strip_tags($_POST['height']);
      $width = strip_tags($_POST['width']);
      $length = strip_tags($_POST['length']);

      $product->insertProduct($sku,$name,$price,$type,null,$height,$width,$length,null);
    }
  }

  if(isset($_GET['checkSKU'])){
    $sku = $_GET['sku'];
    $product->checkProduct();
  }

?>

<!DOCTYPE html>
<html lang="eng">
<head>

  <!-- META TAGS -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- LINKS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">

  <title>Scandiweb Task</title>

  <!-- STYLE -->
  <style>
    body{
      background-color: #DCD2CB;
			font-family: Helvetica;
    }
    hr{
      background-color: #000000;
    }
    #content{
      margin: 5%;
    }
    #head{
      height: 30px;
    }
    #title{
      font-size: 24px;
    }
    #actions{
      float: right;
    }
    #typeDescriptionLabel{
      width: 100%;
      height:5px;
      text-align: center;
    }
    #footer{
			float: right;
			font-size: 14px;
			font-weight: bold;
		}
    .message{
      width: 35%;
      height: 50px;
      margin-left: 20%;
      text-align:center;
      margin-top:-5px;
    }
    .selectFields{
      vertical-align: middle;
    }
    .hidden{
      display: none;
    }
    .type{
      margin-left: 1%;
      width: 31%;
    }
  </style>

</head>
<body>

<!-- MAIN PAGE -->
<form id="add_product" action="<?php echo $_SERVER['PHP_SELF']; ?>?action=addProduct" method="post">
  <div id="content">
    <div id="head">
      <span id="title">Product Add</span>
      <div class="message hidden" id="message">
  			<p id="messageLabel"></p>
  		</div>
      <div id="actions">
        <button type="submit" class="btn btn-primary ">Save</button>
      </div>
    </div>
    <hr>
    <div class="form-group row">
      <label class="col-1 col-form-label">SKU</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkSKU();"  type="text" name="sku" id="sku">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-1 col-form-label">Name</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkName();" type="text" name="name" id="name">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-1 col-form-label">Price</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkPrice();" type="text" name="price" id="price">
      </div>
    </div>
    <div class="form-group row">
      <label class="col-1 col-form-label">Type</label>
      <div class="col-3">
        <select class="form-control" onmouseover="checkType()"  name="type" id="selectType">
          <option>Select Product Type</option>
          <option value="DVD-Disc">DVD-Disc</option>
          <option value="Book">Book</option>
          <option value="Furniture">Furniture</option>
        </select>
      </div>
    </div>
    <div class="alert alert-warning type hidden" id="typeDescription">
      <p id="typeDescriptionLabel"></p>
    </div>
    <div class="form-group row hidden" id="inputSize">
      <label class="col-1 col-form-label">Size</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkSize();" type="text" name="size" id="size">
      </div>
    </div>
    <div class="form-group row hidden" id="inputHeight">
      <label class="col-1 col-form-label">Height</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkHeight();" type="text" name="height" id="height">
      </div>
    </div>
    <div class="form-group row hidden" id="inputWidth">
      <label class="col-1 col-form-label">Width</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkWidth();" type="text" name="width" id="width">
      </div>
    </div>
    <div class="form-group row hidden" id="inputLength">
      <label class="col-1 col-form-label">Length</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkLength();" type="text" name="length" id="length">
      </div>
    </div>
    <div class="form-group row hidden" id="inputWeight">
      <label class="col-1 col-form-label">Weight</label>
      <div class="col-3">
        <input class="form-control" onmouseover="checkWeight();" type="text" name="weight" id="weight">
      </div>
    </div>
    <hr>
    <div id="footer">
      Scandiweb task / Artis Uljanovs / 28.09.2017
    </div>
  </div>
</form>
<!-- SCRIPTS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>

<script>

  function checkType() {

		var e = document.getElementById("selectType");

    if(e.options[ e.selectedIndex ].value == "DVD-Disc"){
      document.getElementById('inputSize').style.display = 'flex';
      document.getElementById('inputHeight').style.display = 'none';
      document.getElementById('inputWidth').style.display = 'none';
      document.getElementById('inputLength').style.display = 'none';
      document.getElementById('inputWeight').style.display = 'none';
      document.getElementById('typeDescription').style.display = 'flex';
      document.getElementById('typeDescriptionLabel').innerHTML = 'Please, insert DVD-Disc size!';
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Type is fine!</div>';
    }
    else if(e.options[ e.selectedIndex ].value == "Book"){
      document.getElementById('inputSize').style.display = 'none';
      document.getElementById('inputHeight').style.display = 'none';
      document.getElementById('inputWidth').style.display = 'none';
      document.getElementById('inputLength').style.display = 'none';
      document.getElementById('inputWeight').style.display = 'flex';
      document.getElementById('typeDescription').style.display = 'flex';
      document.getElementById('typeDescriptionLabel').innerHTML = 'Please, insert Book Weight!';
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Type is fine!</div>';
    }
    else if(e.options[ e.selectedIndex ].value == "Furniture"){
      document.getElementById('inputSize').style.display = 'none';
      document.getElementById('inputHeight').style.display = 'flex';
      document.getElementById('inputWidth').style.display = 'flex';
      document.getElementById('inputLength').style.display = 'flex';
      document.getElementById('inputWeight').style.display = 'none';
      document.getElementById('typeDescription').style.display = 'flex';
      document.getElementById('typeDescriptionLabel').innerHTML = 'Please, insert Furniture Dimensions!';
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Type is fine!</div>';
    }else{
      document.getElementById('inputSize').style.display = 'none';
      document.getElementById('inputHeight').style.display = 'none';
      document.getElementById('inputWidth').style.display = 'none';
      document.getElementById('inputLength').style.display = 'none';
      document.getElementById('inputWeight').style.display = 'none';
      document.getElementById('typeDescription').style.display = 'none';
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Type field must be selected!</div>';
      return false;
    }

  };

  function checkName(){

    var name = document.getElementById("name").value;

    if(name != ''){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Name is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Name field must be filled!</div>';
      return false;
    }
  }

  function checkPrice(){

    var price = document.getElementById("price").value;

    if(isNaN(price)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Price must be only nummbers!</div>';
      return false;
    }else if(price != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Price is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Price field must be filled!</div>';
      return false;
    }
  }

  function checkSize(){

    var size = document.getElementById("size").value;

    if(isNaN(size)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Size must be only nummbers!</div>';
      return false;
    }else if(size != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Size is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Size field must be filled!</div>';
      return false;
    }
  }

  function checkHeight(){

    var height = document.getElementById("height").value;

    if(isNaN(height)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Height must be only nummbers!</div>';
      return false;
    }else if(height != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Height is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Height field must be filled!</div>';
      return false;
    }
  }

  function checkWidth(){

    var width = document.getElementById("width").value;

    if(isNaN(width)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Width must be only nummbers!</div>';
      return false;
    }else if(width != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Width is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Width field must be filled!</div>';
      return false;
    }
  }

  function checkLength(){

    var length = document.getElementById("length").value;

    if(isNaN(length)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Length must be only nummbers!</div>';
      return false;
    }else if(length != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Length is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Length field must be filled!</div>';
      return false;
    }
  }

  function checkWeight(){

    var weight = document.getElementById("weight").value;

    if(isNaN(weight)){
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Weight must be only nummbers!</div>';
      return false;
    }else if(weight != '') {
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">Weight is fine!</div>';
    }else{
      document.getElementById('message').style.display = 'inline-block';
      document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">Weight field must be filled!</div>';
      return false;
    }
  }

  function checkSKU() {

		var sku = document.getElementById("sku").value;

    document.getElementById('message').style.display = 'inline-block';
    document.getElementById('messageLabel').innerHTML = '<div class="alert alert-info">Checking database... </div>';

    if(sku != ''){
       $.ajax(
         { url: "<?php echo $_SERVER['PHP_SELF']; ?>?checkSKU=true?action=select&sku=" + sku,
           type: "get",
           success: function(result){
             if(result == 0){
               document.getElementById('message').style.display = 'inline-block';
               document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">SKU is fine!</div>';
             }else{
               document.getElementById('message').style.display = 'inline-block';
               document.getElementById('messageLabel').innerHTML = '<div class="alert alert-danger">Product with this SKU allready exists!</div>';
               return false;
             }
           }
       });
   }else{
     document.getElementById('message').style.display = 'inline-block';
     document.getElementById('messageLabel').innerHTML = '<div class="alert alert-warning">SKU field must be filled!</div>';
     return false;
   };

  }

  $("#add_product").submit(function(event){

    var error = false;

    var sku = checkSKU();
    var name = checkName();
    var price = checkPrice();
    var type = checkType();
    var size = checkSize();
    var height = checkHeight();
    var width = checkWidth();
    var length = checkLength();
    var weight = checkWeight();

    var type = document.getElementById("selectType");

    if(sku == false || name == false || price == false || type == false){
      error = true;
    }
    if(type != false && type.options[ type.selectedIndex ].value == "DVD-Disc"){
      if(size == false){
        error = true;
      }
    }
    else if(type != false && type.options[ type.selectedIndex ].value == "Book"){
      if(weight == false){
        error = true;
      }
    }
    else if(type != false && type.options[ type.selectedIndex ].value == "Furniture"){
      if(height == false || width == false || length == false){
        error = true;
      }
    }


    if(error == false){

      event.preventDefault();
      var post_url = $(this).attr("action");
      var request_method = $(this).attr("method");
      var form_data = $(this).serialize();

      $.ajax({
        url : post_url,
        type: request_method,
        data : form_data
      }).done(function(response){
        document.getElementById('message').style.display = 'inline-block';
        document.getElementById('messageLabel').innerHTML = '<div class="alert alert-success">You have successfully added new product!</div>';
        });
    }else{
      alert( 'Something is wrong, please check you data!');
    }
  });
</script>

</body>
</html>
